<?php


class ProductView
{
    public function generateDetailsProductView($dataProduct, $reviews, $avgReviewValue)
    {
        ?>
        <!DOCTYPE html>
        <html lang="fr">
        <!--        HEAD-->
        <?php require_once "view/includes/head.html" ?>
        <body>
        <div id="page">
            <!--HEADER-->
            <?php
            require_once "view/includes/header.php";
            ?>
            <div id="mainContainerProductDetail">
                <section>
                    <div id="pictureAndMainDatas">
                        <img src="<?php echo "view/img/" . $dataProduct["Image_Produit"] ?>"
                             alt="<?php echo $dataProduct["Nom_Produit"] ?>" id="productPicture"/>
                        <div id="mainProductDatas">
                            <div id="productRate">
                                <div id="rateStarsAvg">
                                    <?php
                                    for ($i = 0; $i < $avgReviewValue; $i++) {
                                        ?>
                                        <i class="material-icons-round">star</i>
                                        <?php
                                    }

                                    for ($y = $avgReviewValue; $y < 5; $y++) {
                                        ?>
                                        <i class="material-icons-round">star_border</i>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <p id="avgReviewValue"><?php echo "(" . round($avgReviewValue, 1, PHP_ROUND_HALF_UP) . "/5 - " . sizeof($reviews) . " avis)" ?></p>
                            </div>
                            <h1 id="productName"><?php echo $dataProduct["Nom_Produit"] ?></h1>
                            <div id="buyingPart">
                                <p id="productPrice"><?php echo str_replace('.', ',', $dataProduct["Prix_Produit"]) . "€" ?></p>
                                <a href="index.php?addToCart=<?php echo $dataProduct['ID_Produit']; ?>"
                                   class="addToCartButton">
                                    Ajouter au panier
                                    <i class="material-icons-round">add_shopping_cart</i>
                                </a>
                            </div>

                        </div>
                    </div>

                    <div id="secondaryProductDatas">
                        <h2>Description</h2>
                        <p id="productDesc"><?php echo $dataProduct["Description_Produit"] ?></p>
                        <h2>Avis</h2>
                        <div id="reviews">
                            <?php
                            if (isset($reviews) && !empty($reviews)) {
                                foreach ($reviews as $r) {
                                    ?>
                                    <div class="review">
                                        <div class="nameAndRate">
                                            <p class="customerName"><i
                                                        class="material-icons-round">person</i><?php echo $r['Prenom_Client'] . " " . mb_substr($r['Nom_Client'], 0, 1, "UTF-8") . "."; ?>
                                            </p>
                                            <div class="rateStarsReviews">
                                                <?php
                                                for ($i = 0; $i < $r['Valeur_Note']; $i++) {
                                                    ?>
                                                    <i class="material-icons-round">star</i>
                                                    <?php
                                                }

                                                for ($y = $r['Valeur_Note']; $y < 5; $y++) {
                                                    ?>
                                                    <i class="material-icons-round">star_border</i>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>

                                        <p class="reviewText"><?php echo $r['Commentaire_Note'] ?></p>
                                    </div>
                                    <?php
                                }
                            } else {
                                ?>
                                <p class="review">Aucun avis sur ce produit</p>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </section>
            </div>
            <?php
            //        FOOTER
            require_once "view/includes/footer.html";
            ?>
        </div>
        </body>
        </html>
        <?php
    }

    public function generateProductsView($products, $categories)
    {
        ?>
        <!DOCTYPE html>
        <html lang="fr">
        <!--        HEAD-->
        <?php require_once "view/includes/head.html" ?>
        <body>
        <div id="page">
            <!--HEADER-->
            <?php
            require_once "view/includes/header.php";
            ?>
            <?php if (isset($_SESSION['INFO']) && !empty($_SESSION['INFO'])) {
                ?>
                <div id="toast"
                     class="<?php echo $_SESSION['INFO']["type"] ?>"> <?php echo $_SESSION['INFO']["text"]; ?></div>
                <?php
            } ?>
            <div id="mainContainerProducts">
                <section id="filtersAndSorts">
                    <form action="index.php?searchProduct=1" method="POST" id="formSearch">
                        <h1>Rechercher</h1>
                        <div id="searchInputButton">
                            <input type="text" placeholder="Rechercher" name="productSearch" autocomplete="off">
                            <button type="submit"><i class="material-icons-round">search</i></button>
                        </div>
                    </form>
                    <form action="index.php?filterOrSort=1" method="POST" id="formFilters">
                        <div id="filters">
                            <h1>Filtrer</h1>
                            <div class="labelInputFiltersAndSorts">
                                <label>Nouveautés</label><input
                                        type="checkbox" <?php if (isset($_POST['newArrivals']) || isset($_GET['newArrivals'])) {
                                    echo "checked";
                                } ?> name="newArrivals" value="1">
                            </div>
                            <?php
                            foreach ($categories as $c) {
                                ?>
                                <div class="labelInputFiltersAndSorts">
                                    <label><?php echo $c['Nom_Categorie']; ?></label><input
                                            type="checkbox" <?php if (isset($_POST[$c['Nom_Categorie']]) || isset($_GET['category'])) {
                                        if (isset($_GET['category'])) {
                                            if ($_GET['category'] === $c['ID_Categorie']) {
                                                echo "checked";
                                            }
                                        } else {
                                            echo "checked";
                                        }

                                    } ?> name="<?php echo $c['Nom_Categorie']; ?>"
                                            value="<?php echo $c['ID_Categorie']; ?>">
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <hr id="separator">
                        <div id="sorts">
                            <h1>Trier</h1>
                            <div class="labelInputFiltersAndSorts">
                                <label>Par ordre alphabétique</label><input type="radio" value="name"
                                                                            name="sort"
                                    <?php if (!isset($_POST['sort']) || (isset($_POST['sort']) && $_POST['sort'] === "name")) {
                                        echo "checked";
                                    } ?>>
                            </div>
                            <div class="labelInputFiltersAndSorts">
                                <label>Par prix croissant</label><input type="radio" value="priceAsc"
                                                                        name="sort" <?php if (isset($_POST['sort']) && $_POST['sort'] === "priceAsc") {
                                    echo "checked";
                                } ?>>
                            </div>
                            <div class="labelInputFiltersAndSorts">
                                <label>Par prix décroissant</label><input type="radio" value="priceDesc"
                                                                          name="sort" <?php if (isset($_POST['sort']) && $_POST['sort'] === "priceDesc") {
                                    echo "checked";
                                } ?>>
                            </div>
                            <div class="labelInputFiltersAndSorts">
                                <label>Nouveautés en premier</label><input type="radio" value="date"
                                                                           name="sort" <?php if (isset($_POST['sort']) && $_POST['sort'] === "date") {
                                    echo "checked";
                                } ?>>
                            </div>
                            <div class="labelInputFiltersAndSorts">
                                <label>Par popularité</label><input type="radio" value="rate"
                                                                    name="sort" <?php if (isset($_POST['sort']) && $_POST['sort'] === "rate") {
                                    echo "checked";
                                } ?>>
                            </div>
                        </div>
                        <input type="submit" value="Appliquer" class="submitButton" id="applyFilter"/>
                    </form>
                </section>

                <section id="products">
                    <?php
                    if (!empty($products)) {
                        $i = 0.0;
                        foreach ($products as $product) {
                            ?>
                            <div class="productCard" style="animation-delay: <?php echo 0.1 + $i . "s" ?>">
                                <a class="productLink" href="index.php?product=<?php echo $product['ID_Produit']; ?>">
                                    <img class="productPicture"
                                         src="<?php echo "view/img/" . $product['Image_Produit']; ?>"
                                         alt="<?php echo $product['Nom_Produit']; ?>"/>
                                    <div class="productDiv">
                                        <p class="productName"><?php echo $product['Nom_Produit']; ?></p>
                                    </div>
                                </a>

                                <div class="priceAndButton">
                                    <p class="productPrice"><?php echo $product['Prix_Produit'] . "€"; ?></p>
                                    <a href="index.php?addToCart=<?php echo $product['ID_Produit'] . "&category=" . $product['ID_Categorie']; ?>"
                                       class="fastAddToCartButton">
                                        <i class="material-icons">add_shopping_cart</i>
                                    </a>
                                </div>
                            </div>
                            <?php
                            $i += 0.1;
                        }
                    } else {
                        ?>
                        <p>Aucun produit à afficher</p>
                        <?php
                    }

                    ?>
                    <div class="filling-empty-spaces-left"></div>
                    <div class="filling-empty-spaces-left"></div>
                    <div class="filling-empty-spaces-left"></div>

                </section>

                <i id="stickyButton" class="material-icons-round">filter_list</i>
            </div>
            <?php
            //        FOOTER
            require_once "view/includes/footer.html";
            ?>
        </div>
        <!--                SCRIPTS-->
        <?php include 'includes/scripts.html' ?>
        </body>
        </html>
        <?php
    }
}
