<?php


class ConnectionView
{
    public function generateConnectionView()
    {
        ?>
        <!DOCTYPE html>
        <html lang="fr">
    <!--        HEAD-->
    <?php require_once "view/includes/head.html" ?>
    <body>
    <div id="page">
        <!--HEADER-->
        <?php
        require_once "view/includes/header.php";
        ?>
        <div id="mainContainerSignIn">
            <h1 id="pageTitle">Connexion</h1>

            <?php if (isset($_SESSION['INFO']) && !empty($_SESSION['INFO'])) {
                ?>
                <div id="toast"
                     class="<?php echo $_SESSION['INFO']['type'] ?>"> <?php echo $_SESSION['INFO']['text']; ?></div>
                <?php
            } ?>

            <form action="index.php?signIn=1" method="POST" id="signInForm">
                <div class="labelInput">
                    <label for="email">E-mail</label><br>
                    <input type="text" name="email"/>
                </div>

                <div class="labelInput">
                    <label for="password">Mot de passe</label><br>
                    <input type="password" name="password"/>
                </div>

                <div id="validation">
                    <input type="submit" value="Se connecter" class="submitButton" name="validerInscription"/>
                </div>
                <div id="registerLinkSignIn">
                    <p>Pas encore inscrit ? Cliquez </p>
                    <a href="index.php?registration=1">ici</a>
                    <p>!</p>
                </div>
            </form>
        </div>
        <?php
        //        FOOTER
        require_once "view/includes/footer.html";
        ?>
    </div>
    </body>
        <?php
    }
}
