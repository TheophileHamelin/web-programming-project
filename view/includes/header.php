<header>
    <div id="websiteName"><a href="index.php"><span>Sportify.io</span></a></div>

    <div id="linkContainer">
        <?php
        if (isset($_SESSION["LOGIN"])) {
            ?>
            <a class="headerLink" href="index.php?profile=1"><span>Profil</span><i
                    class="material-icons">account_circle</i></a>
            <a class="headerLink" href="index.php?cart=1"><span>Panier</span><i class="material-icons">shopping_cart</i></a>
            <a class="headerLink" href="index.php?logout=1"><span>Déconnexion</span></a>
            <?php
        } else {
            ?>
            <a class="headerLink" href="index.php?signIn=1"><span>Se connecter</span></a>
            <a class="headerLink" href="index.php?registration=1"><span>S'inscrire</span></a>
            <a class="headerLink" href="index.php?cart=1"><span>Panier</span><i class="material-icons">shopping_cart</i></a>
            <?php
        }
        ?>
    </div>
</header>

