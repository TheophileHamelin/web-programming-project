<?php


class RegisterView
{
    public function generateRegisterView()
    {
        ?>
        <!DOCTYPE html>
        <html lang="fr">
        <!--        HEAD-->
        <?php require_once "view/includes/head.html" ?>
        <body>
        <!--HEADER-->
        <?php
        require_once "view/includes/header.php";
        ?>
        <div id="mainContainer">
            <h1 id="pageTitle">Inscription</h1>

            <?php if (isset($_SESSION['INFO']) && !empty($_SESSION['INFO'])) {
                ?>
                <div id="toast"
                     class="<?php echo $_SESSION['INFO']["type"] ?>"> <?php echo $_SESSION['INFO']["text"]; ?></div>
                <?php
            } ?>

            <form action="index.php?registration=1" method="POST" id="registerForm">
                <div id="fields">
                    <fieldset id="identifiers">
                        <legend>Identifiants</legend>
                        <div class="labelInput">
                            <label for="email">E-mail :</label><br>
                            <input type="text" name="email"/>
                        </div>

                        <div class="labelInput">
                            <label for="password">Mot de passe :</label><br>
                            <input type="password" name="password"/>
                        </div>

                        <div class="labelInput">
                            <label for="password2">Confirmation du mot de passe :</label><br>
                            <input type="password" name="password2"/>
                        </div>
                    </fieldset>

                    <fieldset id="informations">
                        <legend>Informations</legend>
                        <div class="labelInput">
                            <label for="firstName">Prénom</label><br>
                            <input type="text" name="firstName"/>
                        </div>

                        <div class="labelInput">
                            <label for="firstName">Nom</label><br>
                            <input type="text" name="lastName"/>
                        </div>

                        <div class="labelInput">
                            <label for="birthday">Date de naissance</label><br>
                            <input type="text" name="birthday" placeholder="JJ/MM/AAAA"/>
                        </div>

                        <div class="labelInput">
                            <label for="address">Adresse :</label><br>
                            <input type="text" name="address"/>
                        </div>

                        <div class="labelInput">
                            <label for="postalCode">Code Postal :</label><br>
                            <input type="text" name="postalCode"/>
                        </div>

                        <div class="labelInput">
                            <label for="town">Ville :</label><br>
                            <input type="text" name="town"/>
                        </div>
                    </fieldset>
                </div>

                <div id="validation">
                    <input type="submit" value="Valider" class="submitButton" name="validerInscription"/>
                </div>
            </form>
        </div>

        <?php
        //        FOOTER
        require_once "view/includes/footer.html";
        ?>
        </body>
    </html>
        <?php
    }
}
