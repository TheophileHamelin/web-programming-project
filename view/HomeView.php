<?php

class HomeView
{
    public function generateHomeView($categories, $products)
    {
        ?>
        <!DOCTYPE html>
        <html lang="fr">
        <!--        HEAD-->
        <?php require_once "view/includes/head.html" ?>
        <body>
        <?php
        require_once "view/includes/header.php";
        ?>

        <div id="mainContainerHomePage">
            <?php if (isset($_SESSION['INFO']) && !empty($_SESSION['INFO'])) {
                ?>
                <div id="toast"
                     class="<?php echo $_SESSION['INFO']["type"] ?>"> <?php echo $_SESSION['INFO']["text"]; ?></div>
                <?php
            } ?>
            <!--                NEW ARRIVALS PANNEL-->
            <a href="index.php?newArrivals=1" class="categoryPanel">
                <div class="category">
                    <figure class="figureCategory">
                        <img class="categoryPicture" src="view/img/newarrivals.jpg" alt="Nouveautés"/>
                        <figcaption class="categoryTitle">Nouveautés</figcaption>
                    </figure>
                </div>
            </a>
            <?php
            //        CATEGORIES PANNELS
            foreach ($categories as $c) {
                ?>
                <a href="index.php?category=<?php echo $c['ID_Categorie']; ?>" class="categoryPanel">
                    <div class="category">
                        <figure class="figureCategory">
                            <img class="categoryPicture" src="<?php echo "view/img/" . $c['Logo_Categorie']; ?>"
                                 alt="<?php echo $c['Nom_Categorie']; ?>"/>
                            <figcaption class="categoryTitle"><?php echo $c['Nom_Categorie']; ?></figcaption>
                        </figure>
                    </div>
                </a>
                <?php
            }
            //RANDOM PRODUCTS
            ?>
            <section id="randomProducts">
                <h1>Vous aimerez peut-être...</h1>
                <div id="products">
                    <?php
                    $i = 0.0;
                    foreach ($products as $product) {
                        ?>
                        <div class="productCard" style="animation-delay: <?php echo 0.1 + $i . "s" ?>">
                            <a class="productLink" href="index.php?product=<?php echo $product['ID_Produit']; ?>">
                                <img class="productPicture" src="<?php echo "view/img/" . $product['Image_Produit']; ?>"
                                     alt="<?php echo $product['Nom_Produit']; ?>"/>
                                <div class="productDiv">
                                    <p class="productName"><?php echo $product['Nom_Produit']; ?></p>
                                </div>
                            </a>

                            <div class="priceAndButton">
                                <p class="productPrice"><?php echo $product['Prix_Produit'] . "€"; ?></p>
                                <a href="index.php?addToCart=<?php echo $product['ID_Produit'] . "&category=" . $product['ID_Categorie']; ?>"
                                   class="fastAddToCartButton">
                                    <i class="material-icons">add_shopping_cart</i>
                                </a>
                            </div>
                        </div>
                        <?php
                        $i += 0.1;
                    }
                    ?>
                </div>
        </div>
        <?php
        //        FOOTER
        require_once "view/includes/footer.html";
        ?>
        </body>
    </html>
        <?php
    }
}
