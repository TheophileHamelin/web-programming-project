let button = document.getElementById("stickyButton");
let filters = document.getElementById("filtersAndSorts");

button.addEventListener("click", () => {
    if (filters.style.display && filters.style.display !== "none") {
        filters.style.display = "none";
    } else {
        filters.style.display = "flex";
    }
});
