//Buttons
let displayProfile = document.getElementById("displayUserInformation");
let displayOrders = document.getElementById("displayOrders");

//Containers
let profileContainer = document.getElementById("profileContainer");
let orderContainer = document.getElementById("ordersContainer");

displayProfile.addEventListener("click", () => {
    profileContainer.style.display = "flex";
    orderContainer.style.display = "none";
});

displayOrders.addEventListener("click", () => {
    profileContainer.style.display = "none";
    orderContainer.style.display = "flex";
});

let orders = document.getElementsByClassName("orderDetails");

let orderProducts;
let arrowIcon;

for (let order of orders) {
    order.addEventListener("click", function () {
        orderProducts = this.nextSibling.nextSibling;
        arrowIcon = this.lastChild.previousSibling.lastChild.previousSibling;

        this.classList.toggle("active");

        if (orderProducts.style.maxHeight) {
            arrowIcon.style.transform = "rotate(0deg)";
            orderProducts.style.maxHeight = null;
        } else {
            arrowIcon.style.transform = "rotate(180deg)";
            orderProducts.style.maxHeight = orderProducts.scrollHeight + "px";
        }
    });
}

//Responsive button

let stickyButton = document.getElementById("stickyButton");
let optionsMenu = document.getElementById("optionsMenu");

stickyButton.addEventListener("click", () => {
    if (optionsMenu.style.display && optionsMenu.style.display !== "none") {
        optionsMenu.style.display = "none";
    } else {
        optionsMenu.style.display = "flex";
    }
});

