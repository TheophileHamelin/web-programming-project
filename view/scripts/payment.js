function init() {
    let creditCardRadio = document.getElementById('creditCard');
    let paypalRadio = document.getElementById('paypal');
    let paypalCart = document.getElementById('paypalChosen');
    let creditCardCart = document.getElementById('creditCardChosen');

    creditCardRadio.addEventListener("click", () => {
        creditCardCart.style.display = "flex";
        paypalCart.style.display = "none";
    });

    paypalRadio.addEventListener("click", () => {
        creditCardCart.style.display = "none";
        paypalCart.style.display = "flex";
    });


}


