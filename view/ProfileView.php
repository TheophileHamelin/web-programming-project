<?php


class ProfileView
{
    public function generateProfileView($dataUser, $orders, $products)
    {
        ?>
        <!DOCTYPE html>
        <html lang="fr">
    <!--        HEAD-->
    <?php require_once "view/includes/head.html" ?>
    <body>
    <div id="page">
        <!--HEADER-->
        <?php
        require_once "view/includes/header.php";

        if (isset($_SESSION['INFO']) && !empty($_SESSION['INFO'])) {
            ?>
            <div id="toast"
                 class="<?php echo $_SESSION['INFO']["type"] ?>"> <?php echo $_SESSION['INFO']["text"]; ?></div>
            <?php
        } ?>

        <h1 id="pageTitle"><?php ?>
            Bienvenue <?php echo $dataUser["Prenom_Client"] . " " . $dataUser["Nom_Client"] ?></h1>
        <div id="mainContainerProfile">

            <section id="optionsMenu">
                <div class="option" id="displayUserInformation">
                    <i class="material-icons-round">build</i>
                    <p>Paramètres du compte</p>
                </div>
                <div class="option" id="displayOrders">
                    <i class="material-icons-round">shopping_cart</i>
                    <p>Commandes</p>
                </div>
            </section>

            <section id="profileContainer">
                <form action="index.php?updateUserData=1" method="POST">
                    <div id="inputs">
                        <div id="identifiers">
                            <h3>Identifiants</h3>
                            <div class="labelInput">
                                <label for="email">E-mail :</label><br>
                                <input class="inputField" type="text" name="email"
                                       value="<?php echo $dataUser["Login_Client"] ?>"/>
                            </div>

                            <div class="labelInput">
                                <label for="password">Mot de passe actuel</label><br>
                                <input id="password" class="inputField" type="password" name="currentPassword"/>
                            </div>

                            <div class="labelInput">
                                <label for="password2">Nouveau mot de passe</label><br>
                                <input id="password2" class="inputField" type="password" name="newPassword"/>
                            </div>

                            <div class="labelInput">
                                <label for="password2">Confirmation</label><br>
                                <input id="password2" class="inputField" type="password" name="newPassword2"/>
                            </div>
                        </div>

                        <div id="miscData">
                            <h3>Informations</h3>
                            <div class="labelInput">
                                <label for="firstName">Prénom</label><br>
                                <input class="inputNameField" type="text" name="firstName"
                                       value="<?php echo $dataUser["Prenom_Client"] ?>"/>
                            </div>

                            <div class="labelInput">
                                <label for="firstName">Nom</label><br>
                                <input class="imputNameField" type="text" name="lastName"
                                       value="<?php echo $dataUser["Nom_Client"] ?>"/>
                            </div>

                            <div class="labelInput">
                                <label for="birthday">Date de naissance</label><br>
                                <input class="inputField" type="text" name="birthday" placeholder="JJ/MM/AAAA"
                                       value="<?php echo $dataUser["DDN_Client"] ?>"/>
                            </div>
                        </div>

                        <div id="address">
                            <h3>Adresse</h3>
                            <div class="labelInput">
                                <label for="address">Adresse :</label><br>
                                <input class="inputField" type="text" name="address"
                                       value="<?php echo $dataUser["Adresse_Client"] ?>"/>
                            </div>

                            <div class="labelInput">
                                <label for="postalCode">Code Postal :</label><br>
                                <input class="inputField" type="text" name="postalCode"
                                       value="<?php echo $dataUser["CP_Client"] ?>"/>
                            </div>

                            <div class="labelInput">
                                <label for="town">Ville :</label><br>
                                <input class="inputField" type="text" name="town"
                                       value="<?php echo $dataUser["Ville_Client"] ?>"/>
                            </div>
                        </div>
                    </div>

                    <div id="submit">
                        <input type="submit" value="Appliquer les changements" class="submitButton"/>
                    </div>
                </form>
            </section>

            <div id="ordersContainer">
                <?php
                if (!empty($orders)) {
                    $i = 0;
                    foreach ($orders as $order) {
                        ?>
                        <div class="orderDetails">
                            <div class="headerAndValues">
                                <p>Numéro</p>
                                <p><?php echo $order["ID_Commande"] ?></p>
                            </div>

                            <div class="headerAndValues">
                                <p>Total</p>
                                <p><?php echo(str_replace('.', ',', $order["total"]) . "€"); ?></p>
                            </div>
                            <div class="headerAndValues">
                                <p>Date</p>
                                <p><?php echo $order["Date_Commande"] ?></p>
                            </div>
                            <div class="headerAndValues">
                                <p></p>
                                <i class="material-icons-round">keyboard_arrow_down</i>
                            </div>


                        </div>
                        <div class="orderProducts">
                            <?php
                            foreach ($products[$i] as $p) {
                                ?>
                                <div class="productDetails">
                                    <a class="productPictureCartLink"
                                       href="index.php?product=<?php echo($p['ID_Produit']); ?>">
                                        <img class="productPictureCart"
                                             src="/view/img/<?php echo($p['Image_Produit']); ?>"
                                             alt="<?php echo($p['Nom_Produit']); ?>">
                                    </a>
                                    <p class="productTitleCart"><?php echo($p['Nom_Produit']); ?></p>
                                    <p class="productPriceCart"><?php echo(str_replace('.', ',', $p['Prix_Produit']) . "€"); ?></p>
                                    <?php
                                    //                                var_dump($p);
                                    if ($p[13]) {
//                                    Product can be commented
                                        ?>
                                        <div class="commentLink">
                                            <a class="submitButton"
                                               href="index.php?review=<?php echo($p['ID_Produit'] . "&userId=" . $p['ID_Client']); ?>">Commenter</a>
                                        </div>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="filling"></div>
                                        <?php
                                    }
                                    ?>
                                    <!--                                <p class="productQuantityCart">Quantité : -->
                                    <?php //echo($products['quantity'][$i]);
                                    ?><!--</p>-->
                                </div>
                                <?php
                            }
                            ?>
                        </div>

                        <?php
                        $i++;
                    }
                } else {
                    ?>
                    <p>Vous n'avez pas de commandes</p>
                    <?php
                }

                ?>
            </div>
            <i class="material-icons-round" id="stickyButton">menu</i>
        </div>

        <?php
        //        FOOTER
        require_once "view/includes/footer.html";
        ?>
    </div>
    <!--                SCRIPTS-->
    <?php include 'includes/scripts.html' ?>
    </body>
        <?php
    }
}
