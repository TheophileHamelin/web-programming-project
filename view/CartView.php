<?php


class CartView
{
    public function generateCartView($products)
    {
        ?>
        <html lang="fr">
        <!--        HEAD-->
        <?php require_once "view/includes/head.html" ?>
        <body>
        <div id="page">
            <!--HEADER-->
            <?php
            require_once "view/includes/header.php";
            ?>
            <?php if (isset($_SESSION['INFO']) && !empty($_SESSION['INFO'])) {
                ?>
                <div id="toast"
                     class="<?php echo $_SESSION['INFO']["type"] ?>"> <?php echo $_SESSION['INFO']["text"]; ?></div>
                <?php
            } ?>
            <div id="mainContainerProductDetail">
                <h1 id="pageTitle">Votre panier</h1>
                <?php
                if (empty($products['products'])) {
                    ?>
                    <p id="emptyCart">Votre panier est vide.</p>
                    <?php
                } else {
                //Computing total price
                $totalPrice = 0;
                for ($i = 0; $i < sizeof($products['products']); $i++) {
                    $totalPrice += $products['products'][$i]['Prix_Produit'] * $products['quantity'][$i];
                }

                ?>
                <div class="displayTotal">
                    <h2>Total</h2>
                    <h2><?php echo str_replace('.', ',', number_format($totalPrice, 2) . "€"); ?></h2>
                </div>
                <section id="cartContent">
                    <?php
                    //Printing cart
                    $i = 0;
                    for ($i = 0; $i < sizeof($products['products']); $i++) {
                        ?>
                        <div class="articleInCart">
                            <a class="productPictureCartLink"
                               href="index.php?product=<?php echo($products['products'][$i]['ID_Produit']); ?>">
                                <img class="productPictureCart"
                                     src="/view/img/<?php echo($products['products'][$i]['Image_Produit']); ?>"
                                     alt="<?php echo($products['products'][$i]['Nom_Produit']); ?>">
                            </a>
                            <p class="productTitleCart"><?php echo($products['products'][$i]['Nom_Produit']); ?></p>
                            <p class="productPriceCart"><?php echo(str_replace('.', ',', $products['products'][$i]['Prix_Produit']) . "€"); ?></p>
                            <p class="productQuantityCart">Quantité : <?php echo($products['quantity'][$i]); ?></p>
                            <a href="index.php?removeFromCart=<?php echo($i); ?>"><i
                                        class="material-icons-round removeButtonCart">remove_shopping_cart</i></a>
                        </div>
                        <?php
                    }
                    }
                    ?>
                </section>
                <?php
                if (!empty($products['products'])) {
                    ?>
                    <div class="displayTotal">
                        <h2>Total</h2>
                        <h2><?php echo str_replace('.', ',', number_format($totalPrice, 2) . "€"); ?></h2>
                    </div>
                    <a href="index.php?payment=1" id="stickyButton">
                        <i class="material-icons-round">done</i>
                    </a>
                    <?php
                }
                ?>
            </div>
            <?php
            //        FOOTER
            require_once "view/includes/footer.html";
            ?>
        </div>
        </body>
        </html>
        <?php
    }
}
