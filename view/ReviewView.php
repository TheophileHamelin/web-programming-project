<?php


class ReviewView
{
    public function generateReviewView($userId, $productId)
    {
        ?>
        <!DOCTYPE html>
        <html lang="fr">
        <!--        HEAD-->
        <?php require_once "view/includes/head.html" ?>
        <body>
        <div id="page">
            <!--HEADER-->
            <?php
            require_once "view/includes/header.php";

            if (isset($_SESSION['INFO']) && !empty($_SESSION['INFO'])) {
                ?>
                <div id="toast"
                     class="<?php echo $_SESSION['INFO']["type"] ?>"> <?php echo $_SESSION['INFO']["text"]; ?></div>
                <?php
            }
            ?>

            <div id="mainContainerReview">
                <h1 id="pageTitle">Votre commentaire</h1>
                <form action="index.php?newReview=<?php echo($productId . "&userId=" . $userId); ?>" method="POST">

                    <div class="labelInput">
                        <label for="reviewValue">Note (Entre 0 et 5)</label>
                        <input id="reviewValue" type="number" min="0" max="5" name="reviewValue"/>
                    </div>

                    <div class="labelInput" id="comment">
                        <label for="reviewText">Commentaire</label>
                        <textarea id="reviewText" name="comment"></textarea>
                    </div>


                    <input class="submitButton" type="submit" value="Confirmer">
                </form>
            </div>
            <?php
            //        FOOTER
            require_once "view/includes/footer.html";
            ?>
        </div>
        </body>
        </html>

        <?php

    }
}
