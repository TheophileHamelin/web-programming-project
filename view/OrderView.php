<?php


class OrderView
{
    public function generatePaymentMethodView($userData)
    {
        ?>
        <html lang="fr">
        <!--        HEAD-->
        <?php require_once "view/includes/head.html" ?>
        <body onload="init()">
        <div id="page">
            <!--HEADER-->
            <?php
            require_once "view/includes/header.php";
            ?>
            <?php if (isset($_SESSION['INFO']) && !empty($_SESSION['INFO'])) {
                ?>
                <div id="toast"
                     class="<?php echo $_SESSION['INFO']["type"] ?>"> <?php echo $_SESSION['INFO']["text"]; ?></div>
                <?php
            } ?>
            <div id="mainContainerPayment">
                <h1 id="pageTitle">Paiement</h1>
                <form action="index.php?validateOrder=1" method="POST" id="formPayment">
                    <section id="deliveryAddress">
                        <h2>Livraison à domicile</h2>
                        <div id="firstNameAndName">
                            <div class="labelInputPayment">
                                <label for="firstName">Prénom </label><input type="text" class="inputText"
                                                                             name="firstName"
                                                                             id="firstName" value="<?php
                                echo($userData["Prenom_Client"]);
                                ?>"/>
                            </div>
                            <div class="labelInputPayment">
                                <label for="name">Nom </label><input id="name" type="text" class="inputText" name="name"
                                                                     value="<?php
                                                                     echo($userData["Nom_Client"]);
                                                                     ?>"/>
                            </div>
                        </div>
                        <div id="contactInformation">
                            <div class="labelInputPayment">
                                <label for="address">Adresse </label><input id="address" type="text" class="inputText"
                                                                            name="address" value="<?php
                                echo($userData["Adresse_Client"]);
                                ?>"/>
                            </div>
                            <div class="labelInputPayment">
                                <label for="cp">Code Postal </label><input id="cp" type="text" class="inputText"
                                                                           name="cp" value="<?php
                                echo($userData["CP_Client"]);
                                ?>"/>
                            </div>
                            <div class="labelInputPayment">
                                <label for="city">Ville </label><input id="city" type="text" class="inputText"
                                                                       name="city" value="<?php
                                echo($userData["Ville_Client"]);
                                ?>"/>
                            </div>
                            <div class="labelInputPayment">
                                <label for="email">E-mail </label><input id="email" type="text" class="inputText"
                                                                         name="email" value="<?php
                                echo($userData["Login_Client"]);
                                ?>"/>
                            </div>
                        </div>
                    </section>
                    <section id="paymentSelection">
                        <h2>Mode de paiement</h2>
                        <div id="paymentModes">
                            <div class="labelInputPayment">
                                <label for="creditCard" class="logoRadio">
                                    <img id="creditCardLogo" src="view/img/creditCard.png" alt="Carte de crédit">
                                    <input type="radio" name="payment" value="creditCard" id="creditCard"/>
                                </label>
                            </div>
                            <div class="labelInputPayment">
                                <label for="paypal" class="logoRadio">
                                    <img id="paypalLogo" src="view/img/paypal.png" alt="Paypal">
                                    <input checked type="radio" name="payment" value="paypal" id="paypal"/>
                                </label>
                            </div>
                        </div>
                        <div id="creditCardChosen">
                            <label for="cardNumber">Numéro de carte</label><input id="cardNumber" type="text"
                                                                                  class="inputText"><br>
                            <label for="cardExpirationDate">Date d'expiration</label><input id="cardExpirationDate"
                                                                                            type="text"
                                                                                            class="inputText"><br>
                            <label for="securityCode">Cryptogramme</label><input id="securityCode" type="text"
                                                                                 class="inputText">
                        </div>
                        <div id="paypalChosen">
                            <p>Vous serez redirigé vers le site de Paypal une fois votre commande validée.</p>
                        </div>
                    </section>
                    <input type="submit" value="Valider la commande" class="submitButton" id="submitButtonPayment"/>
                </form>
            </div>
            <?php
            //        FOOTER
            require_once "view/includes/footer.html";
            ?>
        </div>
        <!--        SCRIPTS-->
        <?php
        require_once "view/includes/scripts.html";
        ?>
        </body>
        </html>
        <?php
    }

    public function generateOrderConfirmationView()
    {
        ?>
        <html lang="fr">
        <!--        HEAD-->
        <?php require_once "view/includes/head.html" ?>

        <body>
        <div id="page">
            <!--HEADER-->
            <?php
            require_once "view/includes/header.php";
            ?>
            <?php if (isset($_SESSION['INFO']) && !empty($_SESSION['INFO'])) {
                ?>
                <div id="toast"
                     class="<?php echo $_SESSION['INFO']["type"] ?>"> <?php echo $_SESSION['INFO']["text"]; ?></div>
                <?php
            } ?>
            <div id="mainContainerOrderValidation">
                <h1 id="pageTitle">Merci pour votre commande</h1>
                <section id="orderConfirmation">
                    <p>Vous trouverez le récapitulatif de votre commande dans votre profil</p>
                    <a href="index.php" class="backToIndex">Retour à l'accueil</a>
                </section>
                <?php
                //        FOOTER
                require_once "view/includes/footer.html";
                ?>
            </div>
        </body>
        </html>
        <?php
    }
}
