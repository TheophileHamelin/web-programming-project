<?php
require_once PATH_VIEW . "/OrderView.php";
require_once PATH_MODEL . "/dao/dao.php";


class OrderControler
{
    private $dao;
    private $orderView;

    public function __construct(Dao $dao)
    {
        $this->dao = $dao;
        $this->orderView = new OrderView();
    }

    public function generatePaymentMethodView($userEmail)
    {
        try {
            $userData = $this->dao->getInformationsCustomer($userEmail);
        } catch (DatabaseException $e) {
            $_SESSION['INFO'] = array("type" => "error", "text" => "Erreur de la base de données");
        } finally {
            $this->orderView->generatePaymentMethodView($userData);
        }
    }

    public function generateOrderConfirmationView()
    {
        $this->orderView->generateOrderConfirmationView();
    }

    public function validateOrder()
    {
        try {
            if (isset($_SESSION['LOGIN'])) {
                if (isset($_SESSION['CART']) && !empty($_SESSION['CART']['products'])) {
                    //User signed in
                    $userData = $this->dao->getInformationsCustomer($_SESSION['LOGIN']);
                    $date = new DateTime();
                    $timestamp = $date->getTimestamp();
                    $orderId = $this->dao->insertNewOrder($userData['ID_Client'], $timestamp)[0];

                    //Order details

                    foreach ($_SESSION['CART']['products'] as $p) {
                        $this->dao->insertNewOrderDetail($orderId, $p['ID_Produit']);

                        //Reset cart
                        $_SESSION['CART'] = null;
                        $_SESSION['INFO'] = array("type" => "success", "text" => "Commande prise en compte");
                    }
                } else {
                    $_SESSION['INFO'] = array("type" => "error", "text" => "Votre panier est vide");
                }
            } else {
                $_SESSION['INFO'] = array("type" => "error", "text" => "Vous n'êtes pas connecté");
            }

        } catch (DatabaseException $e) {
            $_SESSION['INFO'] = array("type" => "error", "text" => "Erreur de la base de données");
        } finally {
            $this->generateOrderConfirmationView();
        }
    }
}
