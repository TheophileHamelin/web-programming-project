<?php
require_once PATH_VIEW . "/ConnectionView.php";
require_once PATH_CONTROLER . "/ctrl/HomeControler.php";
require_once PATH_CONTROLER . "/ctrl/OrderControler.php";
require_once PATH_MODEL . "/dao/dao.php";

class ConnectionControler
{
    private $connectionView;
    private $ctrlOrder;
    private $ctrlHome;
    private $dao;

    //Constructor
    public function __construct(Dao $dao)
    {
        $this->connectionView = new ConnectionView();
        $this->ctrlHome = new HomeControler($dao);
        $this->dao = $dao;
        $this->ctrlOrder = new OrderControler($dao);
    }

    public function generateConnectionView()
    {
        $this->connectionView->generateConnectionView();
    }

    public function signIn($email, $password)
    {
        try {
            $salt = $this->dao->getSalt($email)[0];
            $hash = hash("sha256", $password . $salt);

            if ($this->dao->signIn($email, $hash)) {
                $_SESSION['INFO'] = array("type" => "success", "text" => "Vous êtes maintenant connecté");
                $_SESSION['LOGIN'] = $email;
                if (!isset($_SESSION['CART'])) {
                    $_SESSION['CART'] = array();
                    $_SESSION['CART']['products'] = array();
                    $_SESSION['CART']['quantity'] = array();
                }
            } else {
                $_SESSION['INFO'] = array("type" => "error", "text" => "Identifiants incorrects");
            }
        } catch (DatabaseException $e) {
            $_SESSION['INFO'] = array("type" => "error", "text" => "Erreur de la base de données");
        } finally {
            if (isset($_SESSION['ORDERING'])) {
                $this->ctrlOrder->generatePaymentMethodView($email);
            } else {
                $this->ctrlHome->generateHomeView();
            }
        }
    }

    public function logout()
    {
        unset($_SESSION['LOGIN']);
        $this->ctrlHome->generateHomeView();
        session_destroy();;
    }
}
