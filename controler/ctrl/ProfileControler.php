<?php
require_once PATH_VIEW . "/ProfileView.php";
require_once PATH_MODEL . "/dao/dao.php";

class ProfileControler
{
    private $profileView;
    private $dao;

    public function __construct(Dao $dao)
    {
        $this->dao = $dao;
        $this->profileView = new ProfileView();
    }

    public function generateProfileView($email)
    {
        try {
            $data = $this->dao->getInformationsCustomer($email);
            $orders = $this->dao->getOrdersCustomer($email);
            $productsOrder = array();

            $birthdate = new DateTime();
            $birthdate->setTimestamp($data["DDN_Client"]);
            $data["DDN_Client"] = $birthdate->format("d/m/Y");

            //Filling products of each orders
            foreach ($orders as &$o) {
                //Timestamp to date
                $date = new DateTime();
                $date->setTimestamp($o['Date_Commande']);
                $o['Date_Commande'] = $date->format("d/m/Y");

                //products of order
                $products = $this->dao->getProductsOfOrder($o['ID_Commande']);
                array_push($productsOrder, $products);
            }


            $j = 0;
            //Filling products array to know if the user has already commented it
            foreach ($productsOrder as $products) {
                $i = 0;
                foreach ($products as $p) {
                    $productId = $p['ID_Produit'];

                    if ($this->dao->userAlreadyCommented($data['ID_Client'], $productId)) {
                        array_push($productsOrder[$j][$i], 0);
                    } else {
                        array_push($productsOrder[$j][$i], 1);
                    }
                    $i++;
                }
                $j++;
            }

        } catch (DatabaseException $e) {
            $_SESSION['INFO'] = "Erreur de la base de données";
        } finally {
            $this->profileView->generateProfileView($data, $orders, $productsOrder);
        }
    }

    public function checkPasswordConfirm($password, $passwordConfirm)
    {
        return ($password === $passwordConfirm);
    }

    function generateRandomString($length)
    {
        $string = '';
        $charList = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $max = strlen($charList) - 1;

        for ($i = 0; $i < $length; ++$i) {
            $string .= $charList[random_int(0, $max)];
        }

        return $string;
    }

    public function updateUser($firstName, $lastName, $address, $postalCode, $town, $birthday, $email, $password, $passwordConfirm, $currentPassword)
    {
        try {
            //Sign in check
            if (isset($_SESSION['LOGIN'])) {
                //Authentication check
                $currentLogin = $_SESSION['LOGIN'];
                $salt = $this->dao->getSalt($currentLogin)[0];
                $hash = hash("sha256", $currentPassword . $salt);

                if ($this->dao->signIn($currentLogin, $hash)) {
                    //Empty fields check
                    if (!empty($password) && !empty($passwordConfirm) && !empty($firstName) && !empty($lastName) && !empty($address) && !empty($postalCode) && !empty($town) && !empty($birthday) && !empty($email)) {
                        //Email already exists check
                        if (!$this->dao->userExists($email) || $email === $currentLogin) {
                            //Password confirmation check
                            if ($this->checkPasswordConfirm($password, $passwordConfirm)) {
                                $salt = $this->generateRandomString(30);
                                $hash = hash("sha256", $password . $salt);

                                //Creation of a timestamp for birthday
                                $birthdayDate = DateTime::createFromFormat("d/m/Y", $birthday);
                                $birthdayTimestamp = $birthdayDate->getTimestamp();

                                $this->dao->updateUser($currentLogin, $firstName, $lastName, $address, $postalCode, $town, $birthdayTimestamp, $email, $hash, $salt);
                                $_SESSION['LOGIN'] = $email;
                                $_SESSION['INFO'] = array("type" => "success", "text" => "Mise à jour réussie");


                            } else {
                                $_SESSION['INFO'] = array("type" => "error", "text" => "Les mots de passe sont différents");
                            }
                        } else {
                            $_SESSION['INFO'] = array("type" => "error", "text" => "L'adresse e-mail est déjà utilisée");
                        }
                    } else {
                        $_SESSION['INFO'] = array("type" => "error", "text" => "Un ou plusieurs champs sont vides");
                    }
                } else {
                    $_SESSION['INFO'] = array("type" => "error", "text" => "Erreur d'authentification");
                }
            } else {
                $_SESSION['INFO'] = array("type" => "error", "text" => "Veuillez vous connecter");
            }

        } catch (DatabaseException $e) {
            $_SESSION['INFO'] = array("type" => "error", "text" => "Erreur de la base de données");
        } finally {
            $this->generateProfileView($_SESSION['LOGIN']);
        }
    }
}
