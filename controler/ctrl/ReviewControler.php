<?php
require_once PATH_VIEW . "/ReviewView.php";
require_once PATH_MODEL . "/dao/dao.php";
require_once PATH_CONTROLER . "/ctrl/ProductControler.php";

class ReviewControler
{
    private $dao;
    private $reviewView;
    private $ctrlProfile;

    public function __construct(Dao $dao)
    {
        $this->dao = $dao;
        $this->reviewView = new ReviewView();
        $this->ctrlProfile = new ProfileControler($dao);
    }

    public function generateReviewView($userId, $productId)
    {
        $this->reviewView->generateReviewView($userId, $productId);
    }

    public function createReview($userId, $productId, $reviewValue, $reviewText)
    {
        try {
            if (!$this->dao->userAlreadyCommented($userId, $productId)) {
                $this->dao->insertReview($userId, $productId, $reviewValue, $reviewText);
                $_SESSION['INFO'] = array("type" => "success", "text" => "Commentaire posté");
            } else {
                $_SESSION['INFO'] = array("type" => "error", "text" => "Vous avez déjà commenté ce produit");
            }

        } catch (DatabaseException $e) {
            $_SESSION['INFO'] = array("type" => "error", "text" => "Erreur de la base de données");
        } finally {
            $this->ctrlProfile->generateProfileView($_SESSION['LOGIN']);
        }
    }
}
