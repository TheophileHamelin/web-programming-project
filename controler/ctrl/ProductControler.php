<?php
require_once PATH_VIEW . "/ProductView.php";
require_once PATH_MODEL . "/dao/dao.php";

class ProductControler
{
    private $productView;
    private $dao;

    public function __construct(Dao $dao)
    {
        $this->productView = new ProductView();
        $this->dao = $dao;
    }

    public function generateCategoryProductView($idCategory)
    {
        try {
            $products = $this->dao->getProductsOfCategory($idCategory);
            $categories = $this->dao->getAllCategories();
        } catch (DatabaseException $e) {
            $_SESSION['INFO'] = array("type" => "error", "text" => "Erreur de la base de données");
        } finally {
            $this->productView->generateProductsView($products, $categories);
        }
    }

    public function generateDetailsProductView($productId)
    {
        try {
            $dataProduct = $this->dao->getProductInformations($productId);
            $reviews = $this->dao->getRateProduct($productId);
            $avgReviewValue = $this->dao->getAverageReviewValue($productId);

            $avgReviewValueFloat = floatval($avgReviewValue['avgRating']);

        } catch (DatabaseException $e) {
            $_SESSION['INFO'] = array("type" => "error", "text" => "Erreur de la base de données");
        } finally {
            $this->productView->generateDetailsProductView($dataProduct, $reviews, $avgReviewValueFloat);
        }
    }

    public function generateNewArrivalsView()
    {
        try {
            $categories = $this->dao->getAllCategories();
            $newArrivals = $this->dao->getNewArrivals();
        } catch (DatabaseException $e) {
            $_SESSION['INFO'] = array("type" => "error", "text" => "Erreur de la base de données");
        } finally {
            $this->productView->generateProductsView($newArrivals, $categories);
        }
    }

    public function searchProduct($string)
    {
        try {
            $categories = $this->dao->getAllCategories();
            $products = $this->dao->searchProducts($string);
        } catch (DatabaseException $e) {
            $_SESSION['INFO'] = array("type" => "error", "text" => "Erreur de la base de données");
        } finally {
            $this->productView->generateProductsView($products, $categories);
        }
    }

    public function generateFilterSortProducts()
    {
        try {
            $categories = $this->dao->getAllCategories();

            $products = array();

            if (isset($_POST['Chaussures'])) {
                if (isset($_POST['newArrivals'])) {
                    $resultShoes = $this->dao->getNewArrivalsCategory($_POST['Chaussures']);
                } else {
                    $resultShoes = $this->dao->getProductsOfCategory($_POST['Chaussures']);
                }

                $products = array_merge($products, $resultShoes);
            }

            if (isset($_POST['Vêtements'])) {
                if (isset($_POST['newArrivals'])) {
                    $resultClothes = $this->dao->getNewArrivalsCategory($_POST['Vêtements']);
                } else {
                    $resultClothes = $this->dao->getProductsOfCategory($_POST['Vêtements']);
                }
                $products = array_merge($products, $resultClothes);
            }

            if (isset($_POST['Accessoires'])) {
                if (isset($_POST['newArrivals'])) {
                    $resultAccessories = $this->dao->getNewArrivalsCategory($_POST['Accessoires']);
                } else {
                    $resultAccessories = $this->dao->getProductsOfCategory($_POST['Accessoires']);
                }

                $products = array_merge($products, $resultAccessories);
            }

            if (!isset($_POST['Chaussures']) && !isset($_POST['Vêtements']) && !isset($_POST['Accessoires'])) {
                $products = $this->dao->getAllProducts();
            }

            if (!isset($_POST['Chaussures']) && !isset($_POST['Vêtements']) && !isset($_POST['Accessoires']) && isset($_POST['newArrivals'])) {
                $products = $this->dao->getNewArrivals();
            }


            if (isset($_POST['sort'])) {
                if ($_POST['sort'] === "priceAsc") {
                    $sortProducts = $this->dao->getProductsOrderByPriceAsc();
                } else if ($_POST['sort'] === "priceDesc") {
                    $sortProducts = $this->dao->getProductsOrderByPriceDesc();
                } else if ($_POST['sort'] === "date") {
                    $sortProducts = $this->dao->getProductsOrderByDate();
                } else if ($_POST['sort'] === "rate") {
                    $sortProducts = $this->dao->getProductsOrderByPopularity();
                } else {
                    $sortProducts = $this->dao->getProductsOrderByName();
                }

                $sortProductsIds = array();

                foreach ($sortProducts as $s) {
                    array_push($sortProductsIds, $s[0]);
                }

                $productsIds = array();
                foreach ($products as $p) {
                    array_push($productsIds, $p[0]);
                }

                //Result contains the ID's of the product to keep
                $result = array_intersect($sortProductsIds, $productsIds);

                //If the id of sorted products is not in result, I remove it from sortProducts
                foreach ($sortProducts as $s) {
                    if (!in_array($s[0], $result, TRUE)) {
                        $index = array_search($s, $sortProducts, TRUE);
                        unset($sortProducts[$index]);
                    }
                }

                $products = $sortProducts;
            }

        } catch (DatabaseException $e) {
            $_SESSION['INFO'] = array("type" => "error", "text" => "Erreur de la base de données");
        } finally {
            $this->productView->generateProductsView($products, $categories);
        }
    }
}
