<?php
require_once PATH_VIEW . "/RegisterView.php";
require_once PATH_MODEL . "/dao/dao.php";

//Link between registration interface and database
class RegisterControler
{
    private $registerView;
    private $dao;

//Constructor
    public function __construct(Dao $dao)
    {
        $this->registerView = new RegisterView();
        $this->dao = $dao;
    }

    //Print view to register
    public function generateRegisterView()
    {
        $this->registerView->generateRegisterView();
    }

    public function checkPasswordConfirm($password, $passwordConfirm)
    {
        return ($password === $passwordConfirm);
    }


    function generateRandomString($length)
    {
        $string = '';
        $charList = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $max = strlen($charList) - 1;

        for ($i = 0; $i < $length; ++$i) {
            $string .= $charList[random_int(0, $max)];
        }

        return $string;
    }


    //Proceed to the registration of a user
    public function registerUser($firstName, $lastName, $address, $postalCode, $town, $birthday, $email, $password, $passwordConfirm)
    {
        try {
            //Empty fields check
            if (!empty($password) && !empty($passwordConfirm) && !empty($firstName) && !empty($lastName) && !empty($address) && !empty($postalCode) && !empty($town) && !empty($birthday) && !empty($email)) {
                //Email already exists check
                if (!$this->dao->userExists($email)) {
                    //Password confirmation check
                    if ($this->checkPasswordConfirm($password, $passwordConfirm)) {
                        $salt = $this->generateRandomString(30);
                        $hash = hash("sha256", $_POST["password"] . $salt);

                        //Creation of a timestamp for birthday
                        $birthdayDate = DateTime::createFromFormat("d/m/Y", $birthday);
                        $birthdayTimestamp = $birthdayDate->getTimestamp();
                        $this->dao->registerUser($firstName, $lastName, $address, $postalCode, $town, $birthdayTimestamp, $email, $hash, $salt);

                        $_SESSION['INFO'] = array("type" => "success", "text" => "Inscription réussie");


                    } else {
                        $_SESSION['INFO'] = array("type" => "error", "text" => "Les mots de passe sont différents");
                    }
                } else {
                    $_SESSION['INFO'] = array("type" => "error", "text" => "L'adresse e-mail est déjà utilisée");
                }
            } else {
                $_SESSION['INFO'] = array("type" => "error", "text" => "Un ou plusieurs champs sont vides");
            }
        } catch (DatabaseException $e) {
            $_SESSION['INFO'] = array("type" => "error", "text" => "Erreur de la base de données");
        } finally {
            $this->generateRegisterView();
        }
    }
}
