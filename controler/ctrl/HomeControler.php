<?php
//Home controler : Management of the home page
require_once PATH_VIEW . "/HomeView.php";
require_once PATH_MODEL . "/dao/dao.php";

class HomeControler
{
    private $homeView;
    private $dao;

    //Constructor
    public function __construct(Dao $dao)
    {
        $this->dao = $dao;
        $this->homeView = new HomeView();
    }

    //Printing home view
    public function generateHomeView()
    {
        try {
            $categories = $this->dao->getAllCategories();
            $products = $this->dao->getRandomProducts();
        } catch (DatabaseException $e) {
            $_SESSION['INFO'] = array("type" => "error", "text" => "Erreur de la base de données");
        } finally {
            $this->homeView->generateHomeView($categories, $products);
        }

    }

}
