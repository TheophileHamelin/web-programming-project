<?php
require_once PATH_VIEW . "/CartView.php";
require_once PATH_MODEL . "/dao/dao.php";
require_once PATH_CONTROLER . "/ctrl/ProductControler.php";

class CartControler
{
    private $dao;
    private $cartView;
    private $ctrlProduct;

    public function __construct(Dao $dao)
    {
        $this->dao = $dao;
        $this->cartView = new CartView();
        $this->ctrlProduct = new ProductControler($dao);
    }

    public function generateCartView()
    {
        if (isset($_SESSION['CART'])) {
            $this->cartView->generateCartView($_SESSION['CART']);
        } else {
            $this->cartView->generateCartView(null);
        }

    }

    public function addToCart($productId, $categoryId)
    {
        try {
            $product = $this->dao->getProductInformations($productId);
            //Check if a cart is already created
            if (isset($_SESSION['CART'])) {
                //Check if the product is already in cart
                $isAlreadyInCart = false;
                $i = 0;
                $indexInCart = 0;
//                var_dump($_SESSION['CART']['products']);
                foreach ($_SESSION['CART']['products'] as $p) {
                    if ($p['ID_Produit'] === $productId) {
                        $isAlreadyInCart = true;
                        $indexInCart = $i;
                    }
                    $i++;
                }

                if ($isAlreadyInCart) {
                    $currentQuantity = $_SESSION['CART']['quantity'][$indexInCart];
                    $_SESSION['CART']['quantity'][$indexInCart] = $currentQuantity + 1;
                } else {
                    array_push($_SESSION['CART']['products'], $product);
                    array_push($_SESSION['CART']['quantity'], 1);
                }
            } else {
                $_SESSION['CART'] = array();
                $_SESSION['CART']['products'] = array();
                $_SESSION['CART']['quantity'] = array();
                array_push($_SESSION['CART']['products'], $product);
                array_push($_SESSION['CART']['quantity'], 1);
            }

            $_SESSION['INFO'] = array("type" => "success", "text" => "Article ajouté au panier");
        } catch (DatabaseException $e) {
            $_SESSION['INFO'] = array("type" => "error", "text" => "Erreur de la base de données");
        } finally {
            if (is_null($categoryId)) {
                $this->generateCartView();
            } else {
                $this->ctrlProduct->generateCategoryProductView($categoryId);
            }
        }
    }

    public function removeFromCart($index)
    {
        if (!empty($_SESSION['CART']['products'])) {
            //Checking quantity of the product
            $currentQuantity = $_SESSION['CART']['quantity'][$index];

            if ($currentQuantity > 1) {
                //Quantity > 1 -> reducing the quantity by 1
                $_SESSION['CART']['quantity'][$index] = $currentQuantity - 1;
                $_SESSION['INFO'] = array("type" => "error", "text" => "Un exemplaire a été retiré");
            } else {
                //Quantity = 1 -> deleting product from cart
                array_splice($_SESSION['CART']['quantity'], $index, 1);
                array_splice($_SESSION['CART']['products'], $index, 1);
                $_SESSION['INFO'] = array("type" => "error", "text" => "Article supprimé du panier");
            }
        } else {
            $_SESSION['INFO'] = array("type" => "error", "text" => "Votre panier est vide");
        }

        $this->generateCartView();

    }
}
