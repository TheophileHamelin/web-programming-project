<?php
//REDIRECTS REQUESTS TO CONTROLERS
require_once PATH_CONTROLER . "/ctrl/HomeControler.php";
require_once PATH_CONTROLER . "/ctrl/RegisterControler.php";
require_once PATH_CONTROLER . "/ctrl/ConnectionControler.php";
require_once PATH_CONTROLER . "/ctrl/ProfileControler.php";
require_once PATH_CONTROLER . "/ctrl/ProductControler.php";
require_once PATH_CONTROLER . "/ctrl/CartControler.php";
require_once PATH_CONTROLER . "/ctrl/OrderControler.php";
require_once PATH_CONTROLER . "/ctrl/ReviewControler.php";
require_once PATH_MODEL . "/dao/dao.php";


Class Router
{
    private $dao;
    private $ctrlHome;
    private $ctrlRegister;
    private $ctrlConnection;
    private $ctrlProfile;
    private $ctrlProduct;
    private $ctrlCart;
    private $ctrlOrder;
    private $ctrlReview;

    //Constructor
    public function __construct()
    {
        $this->dao = new Dao();
        $this->ctrlHome = new HomeControler($this->dao);
        $this->ctrlRegister = new RegisterControler($this->dao);
        $this->ctrlConnection = new ConnectionControler($this->dao);
        $this->ctrlProfile = new ProfileControler($this->dao);
        $this->ctrlProduct = new ProductControler($this->dao);
        $this->ctrlCart = new CartControler($this->dao);
        $this->ctrlOrder = new OrderControler($this->dao);
        $this->ctrlReview = new ReviewControler($this->dao);
    }

    public function conductRequest()
    {
        if (isset($_GET['searchProduct'])) {
            //SEARCHING PRODUCT
            if (isset($_POST['productSearch'])) {
                $this->ctrlProduct->searchProduct($_POST['productSearch']);
            } else {
                $this->ctrlProduct->generateNewArrivalsView();
            }
            $_SESSION['INFO'] = NULL;
        } else if (isset($_GET['updateUserData']) && isset($_SESSION['LOGIN'])) {
            //UPDATING PROFILE DATA
            if (isset($_POST['firstName']) && isset($_POST['lastName']) && isset($_POST['address']) && isset($_POST['postalCode']) && isset($_POST['town']) && isset($_POST['birthday']) && isset($_POST['email']) && isset($_POST['newPassword']) && isset($_POST['newPassword2']) && isset($_POST['currentPassword'])) {
                $this->ctrlProfile->updateUser($_POST['firstName'], $_POST['lastName'], $_POST['address'], $_POST['postalCode'], $_POST['town'], $_POST['birthday'], $_POST['email'], $_POST['newPassword'], $_POST['newPassword2'], $_POST['currentPassword']);
            } else {
                $this->ctrlProfile->generateProfileView($_SESSION['LOGIN']);
            }
            $_SESSION['INFO'] = NULL;
        } else if (isset($_GET['newReview']) && isset($_GET['userId'])) {
            //INSERT A REVIEW
            if (isset($_POST['reviewValue']) && isset($_POST['comment']) && !empty($_POST['reviewValue']) && !empty($_POST['comment'])) {
                $this->ctrlReview->createReview($_GET['userId'], $_GET['newReview'], $_POST['reviewValue'], $_POST['comment']);
            } else {
                $this->ctrlProfile->generateProfileView($_SESSION['LOGIN']);
            }
            $_SESSION['INFO'] = NULL;
        } else if (isset($_GET['review']) && isset($_GET['userId'])) {
            //GENERATE REVIEW VIEW
            $this->ctrlReview->generateReviewView($_GET['userId'], $_GET['review']);
            $_SESSION['INFO'] = NULL;
        } else if (isset($_GET['validateOrder'])) {
            //VALIDATING ORDER
            $this->ctrlOrder->validateOrder();
            $_SESSION['INFO'] = NULL;
        } else if (isset($_GET['payment'])) {
            //PAYMENT
            if (isset($_SESSION['LOGIN'])) {
                //User signed in
                $this->ctrlOrder->generatePaymentMethodView($_SESSION['LOGIN']);
            } else {
                //User not signed in
                $_SESSION['ORDERING'] = true;
                $_SESSION['INFO'] = array("type" => "error", "text" => "Veuillez vous connecter");
                $this->ctrlConnection->generateConnectionView();
            }
            $_SESSION['INFO'] = NULL;
        } else if (isset($_GET['removeFromCart'])) {
            //REMOVE SOMETHING FROM CART
            $this->ctrlCart->removeFromCart($_GET['removeFromCart']);
            $_SESSION['INFO'] = NULL;
        } else if (isset($_GET['addToCart'])) {
            //ADD A PRODUCT TO CART
            if (isset($_GET['category'])) {
                $this->ctrlCart->addToCart($_GET['addToCart'], $_GET['category']);
            } else {
                $this->ctrlCart->addToCart($_GET['addToCart'], null);
            }
            $_SESSION['INFO'] = NULL;
        } else if (isset($_GET['cart'])) {
            //CART PAGE
            $this->ctrlCart->generateCartView();
            $_SESSION['INFO'] = NULL;
        } else if (isset($_GET['product'])) {
            //PRODUCT DETAILS PAGE
            $this->ctrlProduct->generateDetailsProductView($_GET['product']);
            $_SESSION['INFO'] = NULL;
        } else if (isset($_GET['category'])) {
            //CATEGORIES PAGE
            $this->ctrlProduct->generateCategoryProductView($_GET['category']);
            $_SESSION['INFO'] = NULL;
        } else if (isset($_GET['filterOrSort'])) {
            //APPLY FILTERS OR SORTS
            $this->ctrlProduct->generateFilterSortProducts();
            $_SESSION['INFO'] = NULL;
        } else if (isset($_GET['newArrivals'])) {
            //NEW ARRIVALS PAGE
            $this->ctrlProduct->generateNewArrivalsView();
            $_SESSION['INFO'] = NULL;
        } else if (isset($_GET['productDetails'])) {
            //PRODUCT INFORMATIONS PAGE
            $this->ctrlProduct->generateDetailsProductView($_GET['productDetails']);
            $_SESSION['INFO'] = NULL;
        } else if (isset($_GET['profile']) && isset($_SESSION['LOGIN'])) {
            //PROFILE PAGE
            $this->ctrlProfile->generateProfileView($_SESSION['LOGIN']);
            $_SESSION['INFO'] = NULL;
        } else if (isset($_GET['logout'])) {
            //LOGOUT
            $this->ctrlConnection->logout();

        } else if (isset($_GET['signIn'])) {
            //SIGN IN
            if (isset($_POST['email']) && isset($_POST['password'])) {
                $this->ctrlConnection->signIn($_POST['email'], $_POST['password']);
            } else {
                $this->ctrlConnection->generateConnectionView();
            }
            $_SESSION['INFO'] = NULL;
        } else if (isset($_GET['registration'])) {
            //REGISTRATION
            if (isset($_POST['firstName']) && isset($_POST['lastName']) && isset($_POST['address']) && isset($_POST['postalCode']) && isset($_POST['town']) && isset($_POST['birthday']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['password2'])) {
                $this->ctrlRegister->registerUser($_POST['firstName'], $_POST['lastName'], $_POST['address'], $_POST['postalCode'], $_POST['town'], $_POST['birthday'], $_POST['email'], $_POST['password'], $_POST['password2']);
            } else {
                $this->ctrlRegister->generateRegisterView();
            }
            $_SESSION['INFO'] = NULL;
        } else {
            //DEFAULT : HOME PAGE
            $this->ctrlHome->generateHomeView();
            $_SESSION['INFO'] = NULL;
        }

    }
}


