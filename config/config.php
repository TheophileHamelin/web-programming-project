<?php

// CHEMIN RACINE
define("HOME_SITE", __DIR__ . "/..");

// CHEMINS VERS REPERTOIRE DU SITE
define("PATH_VIEW", HOME_SITE . "/view");
define("PATH_CONTROLER", HOME_SITE . "/controler");
define("PATH_MODEL", HOME_SITE . "/model");

// DONNEES BASE DE DONNEE
define("HOST", "localhost");

define("BD", "sportify.io");

//User root
define("LOGIN", "root");
define("PASSWORD", "");
