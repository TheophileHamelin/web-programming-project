<?php

//CLASS FOR CUSTOM PHP EXCEPTIONS
class MyExceptionTemplate extends Exception
{
    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    public function print()
    {
        return $this->message;
    }
}
