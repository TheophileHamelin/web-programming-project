<?php

// GENERAL CLASS FOR CUSTOM PDO EXCEPTIONS
class MyPDOExceptionTemplate extends PDOException
{
    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    public function print()
    {
        return $this->message;
    }
}

