<?php

require_once PATH_MODEL . "/exceptionsTemplates/MyPDOExceptionTemplate.php";

//Exceptions
class DatabaseException extends MyPDOExceptionTemplate
{
}

class Dao
{
    private $connection;

//DATABASE
    /* Connection to the database */
    public function __construct()
    {
        try {
            $string = "mysql:host=" . HOST . ";dbname=" . BD . ";charset=UTF8";
            $pdo_options[PDO::MYSQL_ATTR_INIT_COMMAND] = 'SET NAMES utf8';
            $this->connection = new PDO($string, LOGIN, PASSWORD, $pdo_options);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de se connecter à la base de données");
        }
    }

    public function userExists($email)
    {
        try {
            $stmt = $this->connection->prepare("SELECT ID_Client FROM clients WHERE Login_Client = ?");
            $stmt->bindParam(1, $email);
            $stmt->execute();

            $result = $stmt->fetch();

            //If the result is empty, there is no user with this mail address
            return !empty($result);

        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function registerUser($firstName, $lastName, $address, $postalCode, $town, $birthday, $email, $hash, $salt)
    {
        try {
            $stmt = $this->connection->prepare("INSERT INTO clients VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->bindParam(1, $lastName);
            $stmt->bindParam(2, $firstName);
            $stmt->bindParam(3, $address);
            $stmt->bindParam(4, $postalCode);
            $stmt->bindParam(5, $town);
            $stmt->bindParam(6, $birthday);
            $stmt->bindParam(7, $email);
            $stmt->bindParam(8, $hash);
            $stmt->bindParam(9, $salt);

            $stmt->execute();

        } catch (PDOException $e) {
            throw new DatabaseException("Inscription impossible");
        }
    }

    public function getSalt($email)
    {
        try {
            $stmt = $this->connection->prepare("SELECT Sel_Client FROM clients WHERE Login_Client=?");
            $stmt->bindParam(1, $email);
            $stmt->execute();
            return $stmt->fetch();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function signIn($email, $hash)
    {
        try {
            $stmt = $this->connection->prepare("SELECT ID_Client FROM clients WHERE Login_Client=? AND Empreinte_Client = ?");
            $stmt->bindParam(1, $email);
            $stmt->bindParam(2, $hash);
            $stmt->execute();
            $result = $stmt->fetch();

            return !empty($result);

        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function getInformationsCustomer($email)
    {
        try {
            $stmt = $this->connection->prepare("SELECT * FROM clients WHERE Login_Client=?");
            $stmt->bindParam(1, $email);
            $stmt->execute();
            return $stmt->fetch();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function getOrdersCustomer($email)
    {
        try {
            $stmt = $this->connection->prepare("SELECT c.ID_Commande, Date_Commande, SUM(Prix_Produit) as total
                                                            FROM commandes c
                                                            INNER JOIN clients c2 on c.ID_Client = c2.ID_Client
                                                            INNER JOIN details_commandes dc on c.ID_Commande = dc.ID_Commande
                                                            INNER JOIN produits p on dc.ID_Produit = p.ID_Produit
                                                            WHERE c2.Login_Client=?                                                         
                                                            GROUP BY c.ID_Commande;");
            $stmt->bindParam(1, $email);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function getProductInformations($idProduct)
    {
        try {
            $stmt = $this->connection->prepare("SELECT * FROM produits WHERE ID_Produit=?");
            $stmt->bindParam(1, $idProduct);
            $stmt->execute();
            return $stmt->fetch();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function getProductsOfCategory($idCategory)
    {
        try {
            $stmt = $this->connection->prepare("SELECT *
                                                            FROM produits p INNER JOIN categories c ON p.ID_Categorie = c.ID_Categorie
                                                            WHERE c.ID_Categorie=?");
            $stmt->bindParam(1, $idCategory);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function getRateProduct($idProduct)
    {
        try {
            $stmt = $this->connection->prepare("SELECT *
                                                            FROM notes n INNER JOIN produits p on n.ID_Produit = p.ID_Produit
                                                                INNER JOIN clients c ON n.ID_Client = c.ID_Client
                                                            WHERE p.ID_Produit=?");
            $stmt->bindParam(1, $idProduct);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function getAverageReviewValue($idProduct)
    {
        try {
            $stmt = $this->connection->prepare("SELECT AVG(Valeur_Note) AS avgRating
                                                            FROM notes n 
                                                                INNER JOIN produits p on n.ID_Produit = p.ID_Produit
                                                                INNER JOIN clients c ON n.ID_Client = c.ID_Client
                                                            WHERE p.ID_Produit=?");
            $stmt->bindParam(1, $idProduct);
            $stmt->execute();
            return $stmt->fetch();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function getAllCategories()
    {
        try {
            $stmt = $this->connection->prepare("SELECT * FROM categories;");
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function getNewArrivals()
    {
        try {
            $dateOneMonthAgo = new DateTime();
            $dateOneMonthAgo->modify('-1 month');
            $dateOneMonthAgo = $dateOneMonthAgo->getTimestamp();

            $stmt = $this->connection->prepare("SELECT * FROM produits WHERE Date_Produit >= ?;");
            $stmt->bindParam(1, $dateOneMonthAgo);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function getProductsOrderByPriceAsc()
    {
        try {
            $stmt = $this->connection->prepare("SELECT * FROM produits p ORDER BY Prix_Produit, Nom_Produit;");
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function getProductsOrderByDate()
    {
        try {
            $stmt = $this->connection->prepare("SELECT * FROM produits p ORDER BY Date_Produit DESC, Nom_Produit;");
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function getProductsOrderByPriceDesc()
    {
        try {
            $stmt = $this->connection->prepare("SELECT * FROM produits p ORDER BY Prix_Produit DESC, Nom_Produit;");
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function getProductsOrderByPopularity()
    {
        try {
            $stmt = $this->connection->prepare("SELECT *, AVG(Valeur_Note) AS noteMoyenne
                                                FROM produits p
                                                LEFT OUTER JOIN notes n on p.ID_Produit = n.ID_Produit
                                                GROUP BY Nom_Produit
                                                ORDER BY AVG(Valeur_Note) DESC, Nom_Produit;");
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function getProductsOrderByName()
    {
        try {
            $stmt = $this->connection->prepare("SELECT * FROM produits p ORDER BY Nom_Produit;");
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function getAllProducts()
    {
        try {
            $stmt = $this->connection->prepare("SELECT * FROM produits p ORDER BY Nom_Produit;");
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function getNewArrivalsCategory($idCategory)
    {
        try {
            $dateOneMonthAgo = new DateTime();
            $dateOneMonthAgo->modify('-1 month');
            $dateOneMonthAgo = $dateOneMonthAgo->getTimestamp();

            $stmt = $this->connection->prepare("SELECT * FROM produits WHERE Date_Produit >= ? AND ID_Categorie=?;");
            $stmt->bindParam(1, $dateOneMonthAgo);
            $stmt->bindParam(2, $idCategory);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function insertNewOrder($userId, $date)
    {
        try {
            $stmt = $this->connection->prepare("INSERT INTO commandes VALUES (NULL, ?, ?);");
            $stmt->bindParam(1, $userId);
            $stmt->bindParam(2, $date);
            $stmt->execute();

            $stmt = $this->connection->prepare("SELECT LAST_INSERT_ID();");
            $stmt->execute();
            return $stmt->fetch();

        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function insertNewOrderDetail($orderId, $productId)
    {
        try {
            $stmt = $this->connection->prepare("INSERT INTO details_commandes VALUES (NULL, ?, ?);");
            $stmt->bindParam(1, $orderId);
            $stmt->bindParam(2, $productId);
            $stmt->execute();

        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function getRandomProducts()
    {
        try {
            $stmt = $this->connection->prepare("SELECT * FROM produits ORDER BY RAND() LIMIT 4;");
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function userAlreadyCommented($userId, $productId)
    {
        try {
            $stmt = $this->connection->prepare("SELECT * FROM notes WHERE ID_Client = ? AND ID_Produit = ?;");
            $stmt->bindParam(1, $userId);
            $stmt->bindParam(2, $productId);
            $stmt->execute();
            $result = $stmt->fetchAll();

//            var_dump($result);

            if (empty($result)) {
                return false;
            } else {
                return true;
            }
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function insertReview($userId, $productId, $reviewValue, $textReview)
    {
        try {
            $stmt = $this->connection->prepare("INSERT INTO notes VALUES (NULL, ?,?,?,?)");
            $stmt->bindParam(1, $userId);
            $stmt->bindParam(2, $productId);
            $stmt->bindParam(3, $reviewValue);
            $stmt->bindParam(4, $textReview);
            $stmt->execute();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function getProductsOfOrder($orderId)
    {
        try {
            $stmt = $this->connection->prepare("SELECT *
                                                        FROM produits
                                                        INNER JOIN details_commandes dc on produits.ID_Produit = dc.ID_Produit
                                                        INNER JOIN commandes c on dc.ID_Commande = c.ID_Commande
                                                        WHERE c.ID_Commande = ?;");
            $stmt->bindParam(1, $orderId);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }

    public function updateUser($currentEmail, $firstName, $lastName, $address, $postalCode, $town, $birthday, $email, $hash, $salt)
    {
        try {
            $stmt = $this->connection->prepare("UPDATE clients SET 
                                                    Nom_Client = ? ,
                                                    Prenom_Client = ?,
                                                    Adresse_Client = ? ,
                                                    CP_Client = ? ,
                                                    Ville_Client = ? ,
                                                    DDN_Client = ? ,
                                                    Login_Client = ?,
                                                    Empreinte_Client = ?,
                                                    Sel_Client = ?
                                                    WHERE Login_Client = ?;");

            $stmt->bindParam(1, $lastName);
            $stmt->bindParam(2, $firstName);
            $stmt->bindParam(3, $address);
            $stmt->bindParam(4, $postalCode);
            $stmt->bindParam(5, $town);
            $stmt->bindParam(6, $birthday);
            $stmt->bindParam(7, $email);
            $stmt->bindParam(8, $hash);
            $stmt->bindParam(9, $salt);
            $stmt->bindParam(10, $currentEmail);

            $stmt->execute();

        } catch (PDOException $e) {
            throw new DatabaseException("Modification impossible");
        }
    }

    public function searchProducts($string)
    {
        try {
            $stringLike = "%" . $string . "%";
            $stmt = $this->connection->prepare("SELECT * FROM produits WHERE Nom_Produit LIKE ?;");
            $stmt->bindParam(1, $stringLike);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new DatabaseException("Impossible de sélectionner des éléments dans la base");
        }
    }
}
