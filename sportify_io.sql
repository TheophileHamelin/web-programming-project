-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  sam. 25 jan. 2020 à 22:20
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sportify.io`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `ID_Categorie` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nom_Categorie` text COLLATE utf8_unicode_ci NOT NULL,
  `Logo_Categorie` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID_Categorie`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`ID_Categorie`, `Nom_Categorie`, `Logo_Categorie`) VALUES
(2, 'Vêtements', 'clothes.jpg'),
(3, 'Chaussures', 'shoes.jpg'),
(4, 'Accessoires', 'accessories.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `ID_Client` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nom_Client` text COLLATE utf8_unicode_ci NOT NULL,
  `Prenom_Client` text COLLATE utf8_unicode_ci NOT NULL,
  `Adresse_Client` text COLLATE utf8_unicode_ci NOT NULL,
  `CP_Client` text COLLATE utf8_unicode_ci NOT NULL,
  `Ville_Client` text COLLATE utf8_unicode_ci NOT NULL,
  `DDN_Client` bigint(20) NOT NULL,
  `Login_Client` text COLLATE utf8_unicode_ci NOT NULL,
  `Empreinte_Client` text COLLATE utf8_unicode_ci NOT NULL,
  `Sel_Client` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID_Client`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `clients`
--

INSERT INTO `clients` (`ID_Client`, `Nom_Client`, `Prenom_Client`, `Adresse_Client`, `CP_Client`, `Ville_Client`, `DDN_Client`, `Login_Client`, `Empreinte_Client`, `Sel_Client`) VALUES
(7, 'Théophile', 'Hamelin', '3 rue Emile Salmson', '53000', 'LAVAL', 936689484, 'theo.hamelin49@gmail.com', '9e517a9318d7e8ee3189a503eb987b595e6828dedac9415bedbf16392a95950a', 'fgyVswGVgChfmbCyGBxENSWKDNJOVQ'),
(8, 'Théophile', 'Hamelin', '3 rue Emile Salmson', '5300', 'LAVAL', 936740711, 'test', '19c1547d55ac7d9e7ba5c75b39eda8097bc2de07b29463c847905ba19174f2c2', 'EyEYplaUugCiNXylUkpJMYGMCirjLn'),
(9, 'Samuel', 'Lablée', '23, rue des charmes', '49800', 'LOIRE AUTHION', 947404576, 'samuel@gmail.com', '8445f24597e34c4c8c16d09416764530952ce581f39aab231a893a9aea3f8ac3', 'ANNgeqQJraWstkcMxXySfcYMVGngux'),
(10, 'Hamelin', 'Théophile', '3 rue Emile Salmson', '53000', 'LAVAL', 936691012, 'theophileh', '620a8d15168d645d8b48c8da412cc0f62fe2cca89019808fcf6050f7241debbc', 'rLEVNatkSDeOkctHLMMPZTMcGhRFYn');

-- --------------------------------------------------------

--
-- Structure de la table `commandes`
--

DROP TABLE IF EXISTS `commandes`;
CREATE TABLE IF NOT EXISTS `commandes` (
  `ID_Commande` bigint(20) NOT NULL AUTO_INCREMENT,
  `ID_Client` bigint(20) NOT NULL,
  `Date_Commande` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_Commande`),
  KEY `FK_Client_Commandes` (`ID_Client`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `commandes`
--

INSERT INTO `commandes` (`ID_Commande`, `ID_Client`, `Date_Commande`) VALUES
(39, 8, 1579510079),
(40, 8, 1579535533),
(41, 8, 1579537152),
(42, 8, 1579611721),
(43, 8, 1579743033),
(44, 8, 1579743278),
(45, 8, 1579749155),
(46, 10, 1579816206),
(47, 10, 1579816220),
(48, 10, 1579852691),
(49, 10, 1579852718),
(50, 10, 1579892093),
(51, 10, 1579892144),
(52, 9, 1579985222);

-- --------------------------------------------------------

--
-- Structure de la table `details_commandes`
--

DROP TABLE IF EXISTS `details_commandes`;
CREATE TABLE IF NOT EXISTS `details_commandes` (
  `ID_DetailCommande` bigint(20) NOT NULL AUTO_INCREMENT,
  `ID_Commande` bigint(20) NOT NULL,
  `ID_Produit` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_DetailCommande`),
  KEY `FK_Commande_Detail` (`ID_Commande`),
  KEY `FK_Produit_Commandes` (`ID_Produit`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `details_commandes`
--

INSERT INTO `details_commandes` (`ID_DetailCommande`, `ID_Commande`, `ID_Produit`) VALUES
(20, 39, 3),
(21, 39, 4),
(22, 40, 2),
(23, 40, 6),
(24, 41, 5),
(25, 41, 6),
(26, 41, 2),
(27, 42, 1),
(28, 42, 8),
(29, 43, 1),
(30, 43, 8),
(31, 43, 5),
(32, 43, 7),
(33, 43, 6),
(34, 44, 2),
(35, 45, 1),
(36, 46, 2),
(37, 46, 6),
(38, 46, 7),
(39, 47, 2),
(40, 47, 6),
(41, 48, 1),
(42, 48, 8),
(43, 49, 6),
(44, 50, 1),
(45, 50, 4),
(46, 51, 1),
(47, 52, 1);

-- --------------------------------------------------------

--
-- Structure de la table `notes`
--

DROP TABLE IF EXISTS `notes`;
CREATE TABLE IF NOT EXISTS `notes` (
  `ID_Note` bigint(20) NOT NULL AUTO_INCREMENT,
  `ID_Client` bigint(20) NOT NULL,
  `ID_Produit` bigint(20) NOT NULL,
  `Valeur_Note` int(11) NOT NULL,
  `Commentaire_Note` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID_Note`),
  KEY `FK_Note_Client` (`ID_Client`),
  KEY `FK_Produit_Client` (`ID_Produit`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `notes`
--

INSERT INTO `notes` (`ID_Note`, `ID_Client`, `ID_Produit`, `Valeur_Note`, `Commentaire_Note`) VALUES
(1, 7, 1, 5, 'Je trouve ces chaussures très confortables. Je recommande.'),
(2, 9, 1, 3, 'Le produit est abîmé au bout d\'un an d\'utilisation non intensive.'),
(3, 9, 4, 2, 'Pas top.'),
(4, 7, 5, 5, 'J\'adore ce sweat !'),
(5, 9, 5, 4, 'J\'adore aussi ce sweat !'),
(6, 9, 3, 3, 'Sac peu pratique.'),
(7, 7, 3, 5, 'Trop bien ce sac !'),
(8, 8, 4, 5, 'SUPER PRODUIT'),
(9, 9, 4, 2, 'BOF'),
(10, 8, 6, 4, 'test'),
(11, 8, 6, 4, 'test'),
(12, 8, 2, 5, 'SUPER T SHIRT'),
(13, 8, 3, 4, 'Super sac'),
(14, 10, 2, 5, 'J\'adore !');

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

DROP TABLE IF EXISTS `produits`;
CREATE TABLE IF NOT EXISTS `produits` (
  `ID_Produit` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nom_Produit` text COLLATE utf8_unicode_ci NOT NULL,
  `Prix_Produit` decimal(9,2) NOT NULL,
  `ID_Categorie` bigint(20) NOT NULL,
  `Image_Produit` text COLLATE utf8_unicode_ci NOT NULL,
  `Description_Produit` text COLLATE utf8_unicode_ci NOT NULL,
  `Date_Produit` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_Produit`),
  KEY `Fk_Produit_Categorie` (`ID_Categorie`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`ID_Produit`, `Nom_Produit`, `Prix_Produit`, `ID_Categorie`, `Image_Produit`, `Description_Produit`, `Date_Produit`) VALUES
(1, 'REEBOK CROSSFIT NANO 6.0 CVRT', '129.95', 3, 'nano6.png', 'Vous ne montez pas 10 fois d’affilée à la corde pour apprendre à mieux grimper. Vous montez à la corde pour vous surpasser. Ne craignez rien ni personne avec nos CrossFit Nano dernier modèle et leur matériau en Kevlar™ à l\'apparence du papier de verre et leur technologie RopePro+. La forme anatomique a été repensée pour vous aider à aller toujours plus loin.', 1579036684),
(2, 'T-SHIRT REEBOK CROSSFIT®', '27.95', 2, 'tanktopcrossfitgrey.webp', 'Ce T-shirt de CrossFit pour homme garantit un confort impeccable même en plein effort. Avec son jersey léger et sa technologie Speedwick, il vous garde au sec pendant les reps les plus intenses.', 1579036684),
(3, 'SAC DE SPORT UFC', '129.95', 4, 'sacufc.webp', 'Représentez l\'UFC dans tous vos déplacements avec ce sac de sport. Conçu avec un compartiment spécial pour ranger les affaires humides, il comprend également des poches internes vous permettant de garder vos objets personnels en sécurité. Les deux poignées et la bandoulière molletonnée le rendent facile à transporter jusqu\'à la salle de sport.', 1579036684),
(4, 'POIGNETS REEBOK CROSSFIT', '27.95', 4, 'poignetscrossfit1.webp', 'Les WOD les plus exigeants peuvent mettre vos poignets à rude épreuve. Avec notre poignet spécial CrossFit, vous êtes soutenu dans vos efforts. Sa matière élastique le rend à la fois souple et résistant. Ajustez-le à votre poignet grâce à sa fermeture à scratch puis enlevez-le très facilement une fois votre entraînement terminé.', 1579036684),
(5, 'SWEAT À CAPUCHE UFC FIGHT NIGHT BLANK WALKOUT', '89.95', 2, 'sweatufc1.jpg', 'Enfilez vous-aussi le sweat officiel que les combattants d’UFC arborent fièrement les soirs de Fight Night. Avec ses coutures plates, sa matière légère et résistante et sa coupe étudiée, il résiste efficacement, combat après combat.', 1579036684),
(6, 'SHORT REEBOK CROSSFIT® EPIC BASE', '49.95', 2, 'shortorangecrossfit.webp', 'Fixez l’objectif temps de votre prochain WOD. Ce short de training pour homme s’adresse à ceux qui ne pensent qu’à la performance. Fabriqué dans un tissu stretch, il évacue l’humidité pour un effet déperlant ultra efficace. Sa fermeture Cordlock vous permet toutes les audaces.', 123),
(7, 'T-SHIRT REEBOK CROSSFIT® MOVE', '39.95', 2, 'tshirtcrossfitgreen.webp', 'Donnez-vous à fond pour atteindre vos objectifs. Ce T-shirt slim pour homme est conçu dans une matière au toucher coton qui évacue la transpiration pour vos WOD quotidiens. Ses manches chauve-souris vous donnent l\'amplitude de mouvement nécessaire pour vos kipping pull-ups et kettlebell swings. Il est orné de motifs CrossFit à l\'avant et dans le dos.', 123),
(8, 'REEBOK CROSSFIT NANO 8 FLEXWEAVE', '129.95', 3, 'nano8.webp', 'Nous avons créé la Nano 8 pour vous aider à repousser vos limites. Vous profiterez d\'un confort incomparable grâce à la nouvelle construction du talon et aux semelles intermédiaire et intérieure moulées en CMEVA. La protection stratégique à l\'avant-pied permet à ce modèle de résister à l\'épreuve du sport ! Le caoutchouc naturel ultra-résistant à l\'abrasion adhère bien au sol et les rainures de flexion à l\'avant-pied vous offrent plus de souplesse et d\'équilibre.\r\n\r\n', 123),
(9, 'NIKE AIR ZOOM PEGASUS 36', '119.99', 3, 'pegasus36.png', 'L\'emblématique Nike Air Zoom Pegasus 36 est de retour avec plus de perforations et une empeigne dotée de mesh tissé pour une aération ciblée au niveau des zones les plus exposées à la chaleur. Le col au niveau du talon et la languette affinés réduisent le poids de la chaussure sans compromis sur le confort, tandis que les câbles Flywire apparents assurent une tenue ajustée à grande vitesse.', 1579986907),
(10, 'NIKE ZOOM FLY SP', '169.99', 3, 'fly.png', 'Offrez vous un condensé de technologie : légèreté, style et respirabilité grâce à la chaussure Nike Zoom Fly SP pour homme à foulée universelle. Cette édition spéciale de la Nike Zoom Fly est la parfaite alliée pour vos sorties de course à pied sur routes.', 1579986907),
(11, 'HOKA ONE ONE CARBON X', '179.99', 3, 'hoka.jpg', 'Avec sa plaque carbone qui restitue l\'energie emmagasiné lors de l\'impact , vous êtes prêt à battre tous vos records !!', 1579986907),
(12, 'ON RUNNING CLOUD X', '149.99', 3, 'cloudx.png', 'Pour ceux qui aiment courir n’importe où. Des chaussures à emporter partout pour tous les types de courses : des alliées sur lesquelles tu pourras toujours compter.', 1579986907),
(13, 'ASICS GEL KAYANO 23', '129.99', 3, 'kayano23.jpg', 'La chaussure Asics Gel-DS Trainer 23 pour homme convient aux runneurs à foulée universelle à pronatrice sur routes et chemins tracés. Souple et légère, sa nouvelle empeigne vous apporte un maintien ajusté. La nouvelle empeigne Adapt Mesh de la chaussure Asics Gel-DS Trainer 23 vient se mouler à la forme de votre pied tout en vous assurant une excellente respirabilité. La semelle extérieure dotée de picots assure une accroche des plus explosives ! Vos foulée gagnent en propulsion, pour des prises de vitesse pleines de dynamisme. La semelle intermédiaire de la chaussure Asics Gel-DS Trainer 23 est fabriquée avec la fibre FlyteFoam, pour un amorti continu et une foulée des plus aériennes. Combiné à cela, le Gel placé au talon absorbe totalement les impacts. Nous conservons une bonne stabilité au fil des kilomètres grâce au Dynamic Duomax qui assure un excellent guidage de votre foulée, tout en fluidité.', 1579986907),
(14, 'NIKE BALLON DE FOOT ORDEM 4', '119.99', 4, 'ordem4.jpg', 'ballon de football haut de gamme de compétition.', 1579986907),
(15, 'BONNET SPORT ODLO', '15.00', 4, 'odlo.jpg', 'Bonnet de sport.', 1579986907);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `FK_Client_Commandes` FOREIGN KEY (`ID_Client`) REFERENCES `clients` (`ID_Client`);

--
-- Contraintes pour la table `details_commandes`
--
ALTER TABLE `details_commandes`
  ADD CONSTRAINT `FK_Commande_Detail` FOREIGN KEY (`ID_Commande`) REFERENCES `commandes` (`ID_Commande`),
  ADD CONSTRAINT `FK_Produit_Commandes` FOREIGN KEY (`ID_Produit`) REFERENCES `produits` (`ID_Produit`);

--
-- Contraintes pour la table `notes`
--
ALTER TABLE `notes`
  ADD CONSTRAINT `FK_Note_Client` FOREIGN KEY (`ID_Client`) REFERENCES `clients` (`ID_Client`),
  ADD CONSTRAINT `FK_Produit_Client` FOREIGN KEY (`ID_Produit`) REFERENCES `produits` (`ID_Produit`);

--
-- Contraintes pour la table `produits`
--
ALTER TABLE `produits`
  ADD CONSTRAINT `Fk_Produit_Categorie` FOREIGN KEY (`ID_Categorie`) REFERENCES `categories` (`ID_Categorie`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
