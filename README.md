# Sportify.io
## Samuel LABLEE ET Théophile HAMELIN

# Description
E-commerce website made for a web programming project during the third year in ESIEA Laval
Site Web d'e-commerce créé pour un projet de programmation Web en 3ème année à l'ESIEA

Ce site est développé avec l'architecture MVC (Modèle-Vue-COntrôleur)

# Fonctionnalités
- Panier persistant après s'être connecté
- Ergonomie : Accès à l'information optimisé, espace autour des éléments cliquables cliquable également
- Responsive sur l'ensemble des pages du site
- Possibilité de rechercher un produit par son nom
- Filtres et tris sur le contenu actuel de la page de produits
- Possibilité de mettre à jour son profil dans la page "Profil" une fois connecté
- Animation en cascade lors de l'apparition des articles
